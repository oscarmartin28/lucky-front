import { Switch, Route, withRouter } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { setUserSlice } from './app/slices/userSlice';
import { checkSession } from './api/auth';

import Splash from './components/Splash';
import Onboarding from './containers/Onboarding';
import UserForm from './containers/RegisterForm/UserForm/UserForm';
import LoginForm from './containers/LoginForm/LoginForm';
import Adoption from './containers/Adoption/Adoption';
import AdoptionForm from './containers/RegisterForm/AdoptionForm'
import Profile from './containers/Profile/Profile';
import Filters from './containers/Filters/Filters';
import SelectLogin from './components/SelectLogin/SelectLogin';
import AdoptionHome from './components/AdoptionHome/AdoptionHome';
import AdoptionDetail from './containers/AdoptionDetail/AdoptionDetail';
import AdoptionState from './containers/AdoptionState';
import Maps from './containers/Maps/Maps'
import Options from './components/Options';
import SecureRoute from './containers/SecureRoute';
import Navbar from './components/Navbar';
import Filterpet from './containers/Filterpet';
import './App.scss';

const ALLOW_NAVBAR_LINKS = ['/home', '/map', '/adoption', '/profile', '/options'];

function App(props) {

  const [loading, setLoading] = useState(true);

  const dispatch = useDispatch();

  useEffect(() => {
     setTimeout(() => {
        setLoading(false);
     }, 2 * 1000)
     checkUserSession();
   }, []);

  const checkUserSession = async () => {
    try {
      const data = await checkSession();
      delete data.password;
      dispatch(setUserSlice({user: data, hasUser: true}));

    }catch(error) {
      dispatch(setUserSlice({ hasUser: false}));
    }
  };

  if (loading) return <Splash />

  return (
    <div className="App">

      <Switch>
        <SecureRoute exact path="/filterpet" component={Filterpet}/>
        <SecureRoute exact path="/map" component={Maps}/>
        <SecureRoute exact path="/options" component={props => <Options {...props} />}/>
        <SecureRoute exact path="/filters" component={Filters}/>
        <SecureRoute exact path="/profile" component={props => <Profile {...props} />}/>
        <Route exact path="/register" component={props => <UserForm {...props} />}/>
        <Route exact path="/select-login" component={props => <SelectLogin {...props}/>}/>
        <Route exact path="/login" component={props => <LoginForm {...props} />}/>
        <SecureRoute exact path="/adoptiondetail/:id" component={AdoptionDetail}/>
        <SecureRoute exact path="/adoption" component={Adoption}/>
        <SecureRoute exact path="/adoptionstate" component={AdoptionState}/>
        <SecureRoute exact path="/adoption-form" component={AdoptionForm}/>
        <SecureRoute exact path="/home" component={AdoptionHome}/>
        <Route exact path="/" component={Onboarding}/>
      </Switch>
      { ALLOW_NAVBAR_LINKS.indexOf(props.location.pathname)>-1 && <Navbar />} 
    </div>

  );
}

export default withRouter(App);