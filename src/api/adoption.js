const adoptionUrl = 'https://lucky-app-project.herokuapp.com/adoption';

export const adoptionPost = async (data) => {
  const request = await fetch(adoptionUrl, {
    method: "POST",
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Credentials": true,
      "Access-Control-Allow-Origin": "*",
    },
    credentials: 'include',
    body: JSON.stringify(data),
  })
  const response = await request.json();
  if(!request.ok) {
    throw new Error(response.message);
  }
  return response;
}