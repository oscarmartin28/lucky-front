const registerUrl = 'https://lucky-app-project.herokuapp.com/user/register';
const loginUrl = 'https://lucky-app-project.herokuapp.com/user/login';
const checkSessionUrl = 'https://lucky-app-project.herokuapp.com/user/check';
const logoutUrl = 'https://lucky-app-project.herokuapp.com/user/logout';

export const checkSession = async () => {
  const request = await fetch(checkSessionUrl, {
    method: 'GET',
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*"
    },
    credentials: 'include',
  });

  const response = await request.json();
  
  if(!request.ok) {
    throw new Error(response.message);
  }
  if (response.message) {
     throw new Error(response.message);
  }else{
    return response;
  }
  
}

export const register = async (userData) => {
  const request = await fetch(registerUrl, {
    method: "POST",
    headers: {
      "Accept": "application/json",
      "Access-Control-Allow-Credentials": true, 
      "Access-Control-Allow-Origin": "*",
    },
    credentials: 'include', 
    body: userData,

  })
  const response = await request.json();
  if(!request.ok) {
    throw new Error(response.message);
  }
  return response;
}

export const login = async userData => {
  const request = await fetch(loginUrl, {
    method: "POST",
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Credentials": true,
      "Access-Control-Allow-Origin": "*",
    },
    credentials: 'include',
    body: JSON.stringify(userData),
  })
  const response = await request.json();
  if(!request.ok) {
    throw new Error(response.message);
  }
  return response;
}


export const logout = async () => {
  const request = await fetch(logoutUrl, {
    method: 'POST',
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*"
    },
    credentials: 'include',
  });

  const response = await request.json();
  
  if(!request.ok) {
    throw new Error(response.message);
  }

  return response;
}