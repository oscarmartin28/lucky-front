const filterUrl = 'https://lucky-app-project.herokuapp.com/pets/filter';

export const filterGet = async () => {
  const request = await fetch(filterUrl, {
    method: 'GET',
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*"
    },
    credentials: 'include',
  });

  const response = await request.json();
  if(!request.ok) {
    throw new Error(response.message);
  }

  return response;
}