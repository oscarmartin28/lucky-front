import { createSlice } from "@reduxjs/toolkit";

export const adoptionSlice = createSlice({
    name: 'adoption',
    initialState: {
        form: {
            personalData: {
                name: '',
                email: '',
                phone: '',
                dni: '',
                address: '',
                postalCode: '',
                city: '',
            },
            infoAnimal: {
                otherAnimals: '',
                animals: '',
                petFriendly: '',
                whyAdopt: '',
                animalNeeds: '',
                expenses: '',
                food: ''
            },
            infoFamily: {
                typeHome : '',
                rent: '',
                allowPets: '',
                changeHome: '',
                garden: '',
                otherPeople: '',
                wantAdoption: '',
                visitHome: '',
            },
        },
    },
    reducers: {
        setAdoptionSlice: (state, action) => {
            state.form = action.payload.form;
        }
    }
})

export const { setAdoptionSlice } = adoptionSlice.actions;

export default adoptionSlice.reducer;