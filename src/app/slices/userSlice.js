import { createSlice } from "@reduxjs/toolkit";

export const userSlice = createSlice({
    name: 'user',
    initialState: {
        hasUser: false,
        user: {
            email: '',
            username: '',
            password: '',
            picture: '',
            pets: [],
            role: '',
        },
    },
    reducers:{
        setUserSlice: (state, action) => {
            state.hasUser = action.payload.hasUser;
            state.user = action.payload.user;
        },

    }
})

export const { setUserSlice } = userSlice.actions;
export const { selectUser } = (state) => state.user.user;

export default userSlice.reducer;