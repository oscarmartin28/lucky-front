import { configureStore } from '@reduxjs/toolkit';
import userReducer from './slices/userSlice';
import adoptionReducer from './slices/adoptionSlice';

export default configureStore({
  reducer: {
    user: userReducer,
    adoption: adoptionReducer,
  },
});