import { useState } from "react";
import { Link } from "react-router-dom";
import { useSelector } from 'react-redux';
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { EffectFade, Navigation, Pagination } from "swiper";

import Cards from "./Cards";
import Dog from "../../images/home/ilustration-dog.png";
import Asociations from "../../images/home/ilustration-asociations.png";
import Sponsor from "../../images/home/ilustration-sponsor.png";
import Chinchilla from "../../images/home/chinchilla.png";
import Iguana from "../../images/home/iguana.png";
import Dogphoto from "../../images/home/dog.png";

import "swiper/swiper.scss";
import "swiper/components/pagination/pagination.scss";
import "./AdoptionHome.scss";

function AdoptionHome() {

  SwiperCore.use([EffectFade, Navigation, Pagination]);

  const user = useSelector(state => state.user.user);

  const [news] = useState([
    {
      title: "10 Curiosidades sobre las chinchillas",
      img: Chinchilla,
    },
    {
      title: "¿Sabes que comen las iguanas?",
      img: Iguana,
    },
    {
      title: "10 lugares para visitar con tu perro en Madrid",
      img: Dogphoto,
    },
  ]);

  return (
    <>
      <div className="home">
        <div className="home__welcome-msg">
          <h3 className="title__principal--large">¡Bienvenid@ {user.username}!</h3>
        </div>

        <Swiper
          className="swiper-container"
          spaceBetween={10}
          slidesPerView={1}
          pagination={{ clickable: true }}
          onSwiper={(swiper) => console.log(swiper)}
          onSlideChange={() => console.log("slide change")}
        >
          <div>
            <SwiperSlide className="swiper-slide">
              <Link to="/adoption">
                <img className="swiper-slide__img" src={Dog} alt="dog" />
              </Link>
            </SwiperSlide>
            <SwiperSlide className="swiper-slide">
              <Link to="/adoption">
                <img className="swiper-slide__img" src={Asociations} alt="dog" />
              </Link>
            </SwiperSlide>
            <SwiperSlide className="swiper-slide">
              <Link to="/adoption">
                <img className="swiper-slide__img" src={Sponsor} alt="dog" />
              </Link>
            </SwiperSlide>
          </div>
        </Swiper>

        <hr className="home__bar"/>

        <Cards novedades={news} />

        <div className="final-space"></div>

        <div>
        
        </div>
      </div>
    </>
  );
}

export default AdoptionHome;
