import "./Cards.scss";

function Cards(props) {
  return (
    <>
      <h3 className="title-section">Novedades</h3>
      {props.novedades.map((novedad, i) => (
        <div key={i} className="content-div">
          <div className="content-div__notice">
            <img src={novedad.img} alt="novedad" />
            <h3>{novedad.title}</h3>
          </div>
        </div>
      ))}
    </>
  );
}

export default Cards;
