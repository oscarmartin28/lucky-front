import React from "react";
import { withRouter } from 'react-router-dom';

import Foto from '../../images/advertisment/advertisment-foto.png';

import "./Advertisment.scss";

function Advertisment(props) {
  const {history} = props;

  function redirectToForm() {
    history.push('/adoption-form');
  }

  function redirectToAdoption() {
    history.push('/adoption');
  }

  return (
    <div className="advertisment">
      <div className="advertisment__box">
        <h2 className="advertisment__box__titulo">Solicitud de adopción</h2>
        <p className="advertisment__box__description">
          Adoptar es un acto de amor, pero 
          sobre todo una responsabilidad 
          de por vida
        </p>
        <p className="advertisment__box__description">
          Por éste motivo es importante que veas el siguiente vídeo donde te explicamos como va a ser todo el proceso de adopción
        </p>

        <div className="advertisment__box__img">
          <img className="advertisment__box__img__foto" src={Foto} alt="advertisment" />
        </div>

        <h4 className="advertisment__box__question">
          ¿Quieres continuar con el proceso 
          de adopción?
        </h4>

        <div className="advertisment__box__buttons">
          
          <button onClick={redirectToAdoption} className="advertisment__box__buttons__cancelar">Cancelar</button>
          <button onClick={redirectToForm} className="advertisment__box__buttons__continuar">Continuar</button>

        </div>
      </div>
    </div>
  );
}

export default withRouter(Advertisment);
