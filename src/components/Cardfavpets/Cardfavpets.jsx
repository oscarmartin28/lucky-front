import { Link } from 'react-router-dom';

import './Cardfavpets.scss';

function Cardfavpets(props) {
    return (
        <div className="cardfavpets">
            
            <div className="cardfavpets__card">
                <div className="cardfavpets__card__box1">
                    <span className="cardfavpets__card__box1__span">
                        <img className="cardfavpets__card__box1__span__img" src={`.${props.fav.icon}`} alt={props.fav.name}/>
                    </span>
                </div>
                <Link className="cardfavpets__link" to={{
                        pathname: `/adoptiondetail/${props.fav._id}`,
                        state: {...props.fav},
                        }}>
                    <div className="cardfavpets__card__box__2">
                        <p className="cardfavpets__card__box__2__pet">{props.fav.name}</p>
                    </div>
                </Link>
            </div>
        </div>
    )
}

export default Cardfavpets;