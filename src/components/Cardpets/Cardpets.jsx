import { Link, withRouter } from 'react-router-dom';
import { favPost } from '../../api/favs';
import Fav from '../../icons_USED/white-heartfav.png';
import './Cardpets.scss';

function Cardpets(props) {

    async function submitForm (ev) {
        ev.preventDefault();
        try{
            await favPost({petId: props.pets._id});
            window.scrollTo(0, 0);
        } catch(error){
            console.log(error.message);
        }
    }

    return (
        <div className="cardpets">
            <div className="cardpets__box">
                <form onSubmit={submitForm}>
                    <div className="cardpets__box__form">
                        <button className="cardpets__box__form__button" type="submit" >
                            <img className="cardpets__box__form__button__img" src={Fav} alt="fav"/>
                        </button>
                    </div>
                </form>
                <img className="cardpets__box__img" src={props.pets.picture} alt={props.pets.name}/>
                <div className="cardpets__box__title">
                    <Link className="link" to={{
                        pathname: `/adoptiondetail/${props.pets._id}`,
                        state: {...props.pets},
                        }}>
                        <h3 className="title__principal--large">{props.pets.name}</h3>
                    </Link>
                    <p className="cardpets__box__title__city">{props.pets.city}</p>
                </div>
            </div>
        </div>
    )
}

export default withRouter(Cardpets);