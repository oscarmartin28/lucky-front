import {useState} from 'react';

import './Filtercard.scss';

function Filtercard(props) {
    
    const [ active, setActive ] = useState('inactive');

    const isActive = () => {
        if (active==='inactive') {
            setActive('active');
        }else {
            setActive('inactive');
        }
    }

    return (
        <div className={active==='active' ? 'filter-card--active' : 'filter-card'} onClick={isActive}>
            <img className="filter-card__img" src={props.Imagen} alt={props.Name}></img>
            <p className="filter-card__name">{props.Name}</p>
        </div>
    )
}

export default Filtercard;