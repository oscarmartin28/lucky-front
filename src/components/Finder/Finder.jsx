import React, {useState} from 'react';
import Search from '../../icons_USED/green-search.png';
import './Finder.scss';

function Finder(props) {

    const [ namePet, setNamePet] = useState('');

    const filter = (ev) => {
        const value = namePet;
        props.getFilterList(value);
    }

    function handleChangeInput(ev) {
        const value = ev.target.value;
        setNamePet(value);
    }

    return (
        <div className="finder">
            <div className="finder__box">
                <input className="finder__box__input" value={namePet} onChange={handleChangeInput} type="text" name="find" placeholder="Buscar"/>
                <span className="finder__box__span" onClick={filter} >
                    <img className="finder__box__span__img" src={Search} alt="search"/>
                </span>
            </div>
        </div>
    )
}

export default Finder;