import {useState} from 'react';
import {Link} from 'react-router-dom';
import { useSelector } from 'react-redux';

import HomeIcon from "../../icons_USED/green-home.png";
import HomeRedIcon from  "../../icons_USED/red-home.png";
import MapIcon from "../../icons_USED/green-map.png";
import MapRedIcon from "../../icons_USED/red-map.png";
import AdoptionIcon from '../../icons_USED/green-dog.png';
import AdoptionRedIcon from "../../icons_USED/red-dog.png";
import UserIcon from "../../icons_USED/green-user.png";
import UserRedIcon from "../../icons_USED/red-user.png";
import OptionIcon from "../../icons_USED/green-dots.png";
import OptionRedIcon from "../../icons_USED/red-dots.png";

import './Navbar.scss';

function Navbar() {

    const [ active, setActive ] = useState('Home');

      const user = useSelector(state => state.user.user);

    return (
        <nav className="navbar">
            <div className="navbar__box">

                <span className="navbar__box__icon">
                   <Link to="/home">
                        <img 
                        className="navbar__box__icon__img" 
                        src={active === 'Home' ? HomeRedIcon : HomeIcon} 
                        alt="Home" 
                        onClick={() => setActive('Home')} 
                        />
                    </Link>
                </span>
                <span className="navbar__box__icon">
                    <Link to="/map">
                        <img 
                        className="navbar__box__icon__img" 
                        src={active === 'Map' ? MapRedIcon : MapIcon} 
                        alt="Map" 
                        onClick={() => setActive('Map')}
                        />
                    </Link>
                    
                </span>
                <span className="navbar__box__icon">
                    <Link to="/adoption">
                        <img 
                        className="navbar__box__icon__img" 
                        src={active === 'Adoption' ? AdoptionRedIcon : AdoptionIcon}  
                        alt="Adoption" 
                        onClick={() => setActive('Adoption')}
                        />
                    </Link>
                </span>
                <span className="navbar__box__icon--user">
                    <Link to="/profile">
                        <img 
                        className="navbar__box__icon__img--user" 
                        src={user&&user.picture}
                        alt="User" 
                        />
                    </Link>
                </span>
                <span className="navbar__box__icon">
                    <Link to="/options">
                        <img 
                        className="navbar__box__icon__img" 
                        src={active === "Option"  ? OptionRedIcon : OptionIcon} 
                        alt="Option" 
                        onClick={() => setActive("Option")}
                        />
                    </Link>
                </span>

            </div>
        </nav>
    )
}

export default Navbar;