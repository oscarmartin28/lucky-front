import { useDispatch} from 'react-redux';
import { useSelector } from 'react-redux';
import { logout } from '../../api/auth';
import { setUserSlice } from '../../app/slices/userSlice';

import ProfileButton from "../../components/ProfileButton";
import Navbar from "../../components/Navbar";

import Arrow from "../../icons_USED/red-arrowfoward.png";
import Event from "../../icons_USED/events.png";
import Curiosity from "../../icons_USED/blogCopy.png";
import Configuration from "../../icons_USED/confi.png";
import Exit from "../../icons_USED/exit.png";
import Help from "../../icons_USED/help.png";

import "./Options.scss";

function Options() {

  const data = useSelector((state) => state.user.user);

  const dispatch = useDispatch();
  const logoutUser = async () => {
      try {
        await logout();
        dispatch(setUserSlice({
          user: null,
          hasUser: null,
        }))
      }catch(error) {
        dispatch(setUserSlice({
          user: data, 
          hasUser: data,
        }))
      }
  }

  return (
    <div className="option">

      <div className="option__box">

        <ProfileButton Icon={Event} Name="Eventos" />
        <ProfileButton Icon={Curiosity} Name="Curiosidades" />
        <ProfileButton Icon={Help} Name="Ayuda" />
        <ProfileButton Icon={Configuration} Name="Configuración" />

        <div className="option__box__separation"></div>

        <button className="profile-button--logout" onClick={logoutUser}>
          <div className="profile-button__left">
            <img
              className="profile-button__left__img"
              src={Exit}
              alt="exit"
            ></img>
            <p className="profile-button__left__name">Cerrar sesión</p>
          </div>

          <div className="profile-button__right">
            <img
              className="profile-button__right__img"
              src={Arrow}
              alt="arrow"
            ></img>
          </div>
        </button>

      </div>
    </div>
  );
}

export default Options;
