import Arrow from "../../icons_USED/red-arrowfoward.png";

import './ProfileButton.scss';

function ProfileButton(props) {

    const {Icon, Name} = props;

    return (
        <div className="profile-button">
            <div className="profile-button__left">
                <img className="profile-button__left__img" src={Icon} alt="icon"></img>
                <p className="profile-button__left__name">{Name}</p>
            </div>

            <div className="profile-button__right">
                <img className="profile-button__right__img" src={Arrow} alt="arrow"></img>
            </div>
        </div>
    )
}

export default ProfileButton;