import { Link } from "react-router-dom";

import "./SelectLogin.scss";

export default function SelectLogin() {

  return (
    <div className="select-login">
      <div className="select-login__bg">
        <div className="select-login__bg__buttons-container">
          <h2 className="select-login__bg__buttons-container__text">¿Tienes cuenta?</h2>
          <Link to="/login">
          <div className="select-login__bg__buttons-container1">
            <button className="button-principal">Iniciar Sesión</button>
          </div>
          </Link>
          <Link to="/register">
            <button className="button-principal">Crear cuenta</button>
          </Link>
        </div>
      </div>
    </div>
  );
}
