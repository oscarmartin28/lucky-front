import './Splash.scss';

function Splash() {
    return (
        <div className="splash">
            <img className="splash__logo" alt="logo lucky" src="./img/splash/logo-lucky/lucky-logo.png"></img>
            <img className="splash__title" alt="title lucky" src="./img/splash/title/title.png"></img>
        </div>
    )
}

export default Splash;