import {useState, useEffect} from 'react';
import {favGet} from '../../api/favs.js';
import {Link} from  'react-router-dom';

import Finder from '../../components/Finder/Finder';
import Cardfavpets from '../../components/Cardfavpets';
import Cardpets from '../../components/Cardpets';
import Plus from '../../icons_USED/red-plus.png';
import Arrow from '../../icons_USED/red-arrowfoward.png';
import Filter from '../../icons_USED/red-filter.png';

import './Adoption.scss';

function Adoption() {

    const [ buscar, setBuscar ] = useState(false);
    const [ petsList, setPetsList ] = useState ([]);
    const [ filterList, setFilterList ] = useState([]);
    const [ favsList, setFavsList ] = useState([]);

    useEffect(() => {
        getPets();
        addFavs();
    }, [])

    async function addFavs(){
        try {
            const favs = await favGet();
            setFavsList(favs);
        }catch(error) {
            console.log(error);
        }
    }

    async function getPets(){
        await fetch("https://lucky-app-project.herokuapp.com/pets")
        .then(res => res.json())
        .then(response => setPetsList(response));
    }

    function getFilterList(value){
        setBuscar(true);
        let filtered = petsList.filter((pet) => {
            return pet['name'].toUpperCase() === value.toUpperCase();
        });
        
        setFilterList(filtered);
    }

    return (
        <div className="adoption">
            <Finder getFilterList={getFilterList} />

            <div className="adoption__filter-error">
                { buscar && !filterList.length ? (
                    <p className="adoption__filter-error__message">
                        No se ha encontrado ninguna mascota con ese nombre.
                    </p>
                ) : null}
            </div>

            <div className="adoption__box">
                <div className="adoption__box__head">
                    <h3 className="title__principal">Mis Mascotas</h3>
                        <span className="adoption__box__head__span">
                            <img className="adoption__box__head__span__img" src={Plus} alt="añadir"/>
                        </span>
                </div>
                
                <div className="adoption__box__head__bis">
                    <p className="title__sub">Accede al perfil de tus mascotas</p>
                </div>
            </div>
            <div className="adoption__cardsfavpets">

                {favsList.map((fav) => {
                    return ( 
                        <Cardfavpets
                        fav={fav} 
                        key={JSON.stringify(fav)}
                        />
                    )
                })}

            </div>
            <hr className="adoption__line"/>
            <Link to="/adoptionstate" className="adoption__link">
                <div className="adoption__barstate">
                    <p className="title__principal--medium">Estado de la Adopción</p>
                    <span className="adoption__barstate__span">
                        <img className="adoption__barstate__span__img" src={Arrow} alt="arrow foward"/>
                    </span>
                </div>
            </Link>
            <div className="adoption__animals">
                <div className="adoption__animals__title">
                    <h3 className="title__principal--medium">Animales en adopción</h3>
                    <Link to="/filters" className="adoption__animals__title__link">
                        <span >
                            <img className="adoption__animals__title__span__img" src={Filter} alt="filter"/>
                        </span>
                    </Link>
                </div>
            </div>
            

            <div className="adoption__animalslist">

            {filterList.length ? (
                <>
                {filterList.map((pets) => {
                    return (<Cardpets 
                    className="adoption__animalslist_list" 
                    pets={pets} 
                    key={Object.keys(pets)}/>)}
                )}
                </>
            ) : (
                <>
                {petsList.map((pets) => {
                    return (<Cardpets 
                    className="adoption__animalslist_list" 
                    pets={pets} 
                    key={JSON.stringify(pets)}/>)}
                )}
                </>
            )} 
                
            </div>
        </div>
    )
}

export default Adoption;