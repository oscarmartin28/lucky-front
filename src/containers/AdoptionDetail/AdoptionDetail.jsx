import { useState } from 'react';
import {Link} from 'react-router-dom';
import {FacebookShareButton, TwitterShareButton, WhatsappShareButton} from 'react-share';
import {
    FacebookIcon,
    TwitterIcon,
    WhatsappIcon,
} from 'react-share';
import { favPost } from '../../api/favs';

import Tabs from '../Tabs/Tabs';
import Advertisment from '../../components/Advertisment';

import Arrow from '../../icons_USED/red-arrowback.png';
import Fav from '../../icons_USED/white-heartfav.png';
import Share from '../../icons_USED/red-share.png';
import Close from '../../icons_USED/green-close.png';
import Foto from '../../images/advertisment/advertisment-foto.png';

import './AdoptionDetail.scss';

function AdoptionDetail(props) {

    const [ showModal, setShowModal ] = useState(false);
    const [isAddInfo, setIsAddInfo] = useState(false);

    function changeModal() {
        setShowModal(true);
    }

    if (!props.location.state) return props.history.push("/adoption");

    const { picture, name, city } = props.location.state;

    async function submitForm (ev) {
        ev.preventDefault();
        const { history } = props;
        try{
            await favPost({petId: props.location.state._id});
            history.push('/adoption');
        } catch(error){
            console.log(error.message);
        }
    }

    const closeInfo = () => {
    setIsAddInfo(false);
    }


    return (
        <div className="adoptiondetail">
            <div className="adoptiondetail__box">
                <span className="adoptiondetail__box__span">
                    <Link className="link" to="/adoption">
                        <img className="adoptiondetail__box__span__img" src={Arrow} alt="arrow back"/>
                    </Link>
                </span>
                <img className="adoptiondetail__box__img" src={picture} alt={name}/>
                <div className="adoptiondetail__box__card">
                    <div className="adoptiondetail__box__card__title">
                        <p className="adoptiondetail__box__card__title__text">{name}</p>
                        <p className="adoptiondetail__box__card__title__text2">{city}</p>
                    </div>

                    <form onSubmit={submitForm} className="adoptiondetail__box__card__form__icons">
                        <div className="adoptiondetail__box__card__form__icons__spanfav">
                            <button className="adoptiondetail__box__card__form__icons__spanfav__button" type="submit" >
                                <img className="adoptiondetail__box__card__form__icons__spanfav__img" src={Fav} alt="fav"/>
                            </button>
                        </div>
                        <div className="adoptiondetail__box__card__form__icons__spanshare">         
                            <span onClick={() => setIsAddInfo({ isAddInfo: true })}>
                                <img 
                                className="adoptiondetail__box__card__form__icons__spanshare__img--share" 
                                src={Share} 
                                alt="share"/>
                            </span>
                        </div>
                    </form>
                    {isAddInfo && 
                            (<div className="modal__share">
                                <FacebookShareButton
                                  url="www.facebook.com"
                                  quote={name}>
                                  <button className="modal__share__button"><FacebookIcon size={32}/></button>
                                </FacebookShareButton>                      

                                <TwitterShareButton
                                  url="www.twitter.com"
                                  title={name}>
                                  <button className="modal__share__button"><TwitterIcon size={32}/></button>
                                </TwitterShareButton>

                                <WhatsappShareButton
                                  url="www.whatsapp.com"
                                  title={name}>
                                  <button className="modal__share__button"><WhatsappIcon size={32}/></button>
                                </WhatsappShareButton>

                                <span className="modal__share__span--close" onClick={closeInfo}>
                                    <img className="modal__share__span__img--close" src={Close} alt="close"/>
                                </span> 
                            </div>)
                        }
                </div>
            </div>

            { showModal && <Advertisment foto={Foto} />}

            <Tabs pets={props}/>
             <div className="adoptiondetail__buttons">
                <div className="adoptiondetail__button1">
                    <button className="button-salmon--white">Apadrinar</button>
                </div>
                <div className="adoptiondetail__button2">
                    <button onClick={changeModal} className="button-salmon">Adoptar</button>
                </div>
            </div> 

        </div>
    )
}

export default AdoptionDetail;