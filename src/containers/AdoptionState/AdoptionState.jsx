import React from "react";
import { Link } from "react-router-dom";

import Navbar from "../../components/Navbar";
import Finder from "../../components/Finder/Finder";

import Arrow from "../../icons_USED/arrow-back.png";
import RedDot from "../../icons_USED/red-dot-small.png";
import YellowDot from "../../icons_USED/yellow-dot.png";
import Filter from "../../icons_USED/red-filter.png";
import Benito from "../../images/adoptionstate/gato-Benito.jpg";
import Ruby from "../../images/adoptionstate/conejo-Ruby.jpg";

import "./AdoptionState.scss";

function AdoptionState() {
  return (
    <div className="estado-adopcion">
      <div className="buscar-estado-adopcion">
        <div className="buscador">
          <Finder/>
        </div>
        <span className="adopcion-lineas-verticales">

        </span>
        <Link to="/adoption">
          <span className="adopcion-goback">
            <img className="imagen-goback" src={Arrow} alt="arrow"></img>
          </span>
        </Link>
        <div className="div-adopcion">
          <span className="texto-adopcion">
            <p className="text-adopcion">Adopción de Benito </p>
          </span>
          <span className="texto-adopcion">
            <p className="text-adopcion-process">
              En proceso <img src={YellowDot} alt="point"></img>
            </p>
          </span>
          <span className="adopcion-proceso">
            <img className="imagen-proceso" src={Benito} alt="mascota"></img>
          </span>
          <div className="datos-proceso">
            <div className="nombre-proceso">Nombre: Benito</div>
            <div className="ciudad-proceso">Ciudad: Madrid</div>
            <div className="sexo-proceso">Sexo: Macho</div>
          </div>
        </div>
        <div className="div-adopcion">
          <span className="texto-adopcion">
            <p className="text-adopcion">Adopción de Ruby</p>
          </span>
          <span className="texto-adopcion">
            <p className="text-adopcion-rechazado">
              Rechazado <img src={RedDot} alt="point"></img>
            </p>
          </span>
          <span className="adopcion-proceso">
            <img className="imagen-proceso" src={Ruby} alt="mascota"></img>
          </span>
          <div className="datos-proceso">
            <div className="nombre-proceso">Nombre: Ruby </div>
            <div className="ciudad-proceso">Ciudad: Madrid</div>
            <div className="sexo-proceso">Sexo: Hembra</div>
          </div>
        </div>
      </div>
      <Navbar />
    </div>
  );
}

export default AdoptionState;
