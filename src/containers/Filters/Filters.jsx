import { Link } from 'react-router-dom';

import Filtercard from '../../components/Filtercard';

import Close from '../../icons_USED/green-close.png';
import Amphibian from "../../icons_USED/filters/amphibian.png";
import BigSizeDog from "../../icons_USED/filters/big-size-dog.png";
import Ferret from "../../icons_USED/filters/ferret.png";
import Fish from "../../icons_USED/filters/fish.png";
import GreenBird from "../../icons_USED/filters/green-bird.png";
import GreenDog from "../../icons_USED/filters/green-dog.png";
import GreenFemale from "../../icons_USED/filters/green-female.png";
import GreenMale from "../../icons_USED/filters/green-male.png";
import MediumSizeDog from "../../icons_USED/filters/green-medium-size-dog.png";
import Guineapig from "../../icons_USED/filters/guineapig.png";
import Mammal from "../../icons_USED/filters/mammal.png";
import Rabbit from "../../icons_USED/filters/rabbit.png";
import RedBird from "../../icons_USED/filters/red-bird.png";
import RedCat from "../../icons_USED/filters/red-cat.png";
import RedDog from "../../icons_USED/filters/red-dog.png";
import RedFemale from "../../icons_USED/filters/red-female.png";
import RedMale from "../../icons_USED/filters/red-male.png";
import RedMediumSizeDog from "../../icons_USED/filters/red-medium-size-dog.png";
import Reptile from "../../icons_USED/filters/reptile.png";
import SmallSizeDog from "../../icons_USED/filters/small-size-dog.png";
import Cat from "../../icons_USED/filters/cat.png";
import Spider from "../../icons_USED/filters/spider.png";

import './Filters.scss';

function Filters() {

    const edades = ['Bebe', 'Joven', 'Adulto', 'Anciano'];

    return (
        <div className="filters">

            <span className="filters__close">
                <Link to="/adoption">
                    <img className="filters__close__img" src={Close} alt="close"/>
                </Link>
            </span>

            <h3 className="filters-title">Filtros</h3>
            
            <form className="filters-form">
                <div className="filters-form__ciudad">
                    <h3 className="filters-form__ciudad__title">Ciudad</h3>
                    <input
                     className="filters-form__ciudad__input" 
                     type="text" 
                     placeholder="Madrid" 
                     >
                     </input>
                </div>

                <div className="filters-form__especie">
                    <h3 className="filters-form__especie__title">Especie</h3>

                    <div className="filters-form__especie__card">
                        <Filtercard Imagen={GreenDog} Name='Perro' />
                        <Filtercard Imagen={Cat} Name='Gato' />
                        <Filtercard Imagen={Rabbit} Name='Conejo' />
                        <Filtercard Imagen={Guineapig} Name='Cobaya' />
                        <Filtercard Imagen={Mammal} Name='Pequeño mamífero' />
                        <Filtercard Imagen={Ferret} Name='Hurón' />
                        <Filtercard Imagen={Fish} Name='Pez' />
                        <Filtercard Imagen={Reptile} Name='Reptil' />
                        <Filtercard Imagen={Amphibian} Name='Anfibio' />
                        <Filtercard Imagen={Spider} Name='Arácnido o insecto' />
                        <Filtercard Imagen={GreenBird} Name='Ave' />
                    </div>
                </div>

                <div className="filters-form__edad">
                    <h3 className="filters-form__edad__title">Edad</h3>

                    { edades ? (

                        <select className="filters-form__edad__select">
                        { edades.map(edad => {
                        return(
                        <option key={edad} value={edad}>{edad}</option>
                        )
                        })}
                        </select>

                    ): null}

                </div>

                <div className="filters-form__sexo">
                    <h3 className="filters-form__sexo__title">Sexo</h3>
                    
                    <div className="filters-form__sexo__card">
                        <Filtercard Imagen={GreenFemale} Name='Hembra' />
                        <Filtercard Imagen={GreenMale} Name='Macho' />
                    </div>
        
                </div>

                <div className="filters-form__tamaño">
                    <h3 className="filters-form__tamaño__title">Tamaño</h3>

                    <div className="filters-form__tamaño__card">
                        <Filtercard Imagen={SmallSizeDog} Name='Pequeño' />
                        <Filtercard Imagen={MediumSizeDog} Name='Mediano' />
                        <Filtercard Imagen={BigSizeDog} Name='Grande' />
                    </div>
                </div>
                
                <button type="submit" className="button-secondary--red">Aplicar</button>
                <br></br>
            </form>
        </div>
    )
}

export default Filters;