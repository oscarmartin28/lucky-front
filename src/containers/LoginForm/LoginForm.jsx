import { Link } from 'react-router-dom';
import { useState } from 'react'
import { useDispatch} from 'react-redux';
import { setUserSlice } from '../../app/slices/userSlice';
import { login } from '../../api/auth';

import Eye from '../../icons_USED/green-eye.png';

import './LoginForm.scss';

const INITIAL_FORM = {
    email: '',
    password: '',
    error: '',
}

function LoginForm(props) {

    const dispatch = useDispatch();

    const [state, setState] = useState(INITIAL_FORM);

    const [passwordShown, setPasswordShown] = useState(false);


    async function handleSubmitForm (ev) {
        ev.preventDefault();
           const { history } = props;
        try {
            const data = await login(state);
            dispatch(setUserSlice({
                user: data,
                hasUser: true,
            }));
            setState(INITIAL_FORM);
            history.push('/home');

        }catch(error) {
            setState({error: error.message});
        };
    };

    function handleChangeInput (ev)  {
        const name = ev.target.name;
        const value = ev.target.value;
        setState({
            ...state,
            [name]: value,
        })
    };

    const togglePasswordVisiblity = () => {
        setPasswordShown(passwordShown ? false : true);
      };

    return (
        <div className="loginform">

            <div className="loginform__box">

                <div className="loginform__box__img" >
                    <img className="loginform__box__img__image" src="/img/logo/logo-M.png" alt="logo lucky"/>
                </div>

                <form className="loginform__form" onSubmit={handleSubmitForm}>

                    <h3 className="title__principal">¡Hola! para continuar, inicia sesión</h3>

                    <label className="loginform__form__label" htmlFor="email">
                        <input className="loginform__form__label__input" type="text" name="email" value={state.email} onChange={handleChangeInput} placeholder="email@email.com"/>
                    </label>

                    <label className="loginform__form__label" htmlFor="password">
                        <input className="loginform__form__label__input" type={passwordShown ? "text" : "password"} name="password" value={state.password} onChange={handleChangeInput} placeholder="******"/>
                    </label>
                        <span className="loginform__form__label__icon"><img className="loginform__form__label__icon__eye" onClick={togglePasswordVisiblity} src={Eye} alt="visibilidad contrañeña"/></span>

                    <p>¿Has olvidado tu contraseña?</p>
                    {/*Pendiente añadir funcionalidad si da tiempo de resetear contraseña https://serverless-stack.com/chapters/handle-forgot-and-reset-password.html */}

                    {state.error && <p className="error">Ha ocurrido un error: {state.error}</p>}

                    <div className="loginform__form__button">
                        <button className="button-principal--white" type="submit">Entrar</button>
                    </div>
                </form>
                <Link className="link" to="/select-login">Volver</Link>
            </div>
        </div>
    )
}

export default LoginForm;