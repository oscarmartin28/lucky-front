import React from "react";
import GoogleMaps from "simple-react-google-maps";
import Finder from '../../components/Finder/Finder';
import "./Maps.scss";

function Maps() {
  return (
    <div className="buscador">
      <div className="input-buscador">
        <Finder/>
      </div>

      <div className="container-mapa">
        <GoogleMaps
          apiKey={"AIzaSyB7VM4e-1REQNV1_S9lmSGuQ4Hg0Y-xR5w"}
          style={{ height: "580px", width: "414px" }}
          zoom={12}
          center={{
            lat: 40.4167,
            lng: -3.70325,
          }}
          markers={
            [{ lat: 40.44737676576417, lng: -3.706960001477945 },
            { lat: 40.430309487378814, lng: -3.702659446117307 },
            { lat: 40.437426229311406, lng: -3.6836256743398463 },
            { lat: 40.39556219915243, lng: -3.7094492732883975 },
            { lat: 40.39437538962063, lng: -3.6653710648363487 },
            { lat: 40.4232767288692, lng: -3.679729873033665 }]
          }
        />
      </div>
    </div>
  );
}

export default Maps;
