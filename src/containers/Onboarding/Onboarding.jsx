import React, {useState} from 'react';
import { Link } from 'react-router-dom';

import Dot from '../../icons_USED/white-dot.png';
import RedDot from '../../icons_USED/red-dot.png';
import Close from '../../icons_USED/green-close.png';

import './Onboarding.scss';

function Onboarding() {

    const [ active, setActive ] = useState('one');

    let onboardingLogo = './img/onboarding/step-1/ilustration.png';
    let onboardingText = 'Encuentra todo tipo de servicios que tienes cerca de ti';
    let onboardingSmallText = 
    'Puedes acceder al perfil de muchos animales que están en adopcion y filtrarlos para encontrar el que mejor se adapte a ti.';
    //const {onboardingLogo, onboardingText, onboardingSmallText} = props;

    if (active === 'one') {

        onboardingLogo = './img/onboarding/step-1/ilustration.png';
        onboardingText = 'Encuentra todo tipo de servicios que tienes cerca de ti';
        onboardingSmallText = null;
    
    }else if (active === 'two') {

        onboardingLogo = './img/onboarding/step-2/ilustration.png';
        onboardingText = 'Adopta desde tu movil';
        onboardingSmallText = 'Puedes acceder al perfil de muchos animales que están en adopcion y filtrarlos para encontrar el que mejor se adapte a ti.';
    
    }else if ( active === 'three') {

        onboardingLogo = './img/onboarding/step-3/ilustration.png';
        onboardingText = 'Si eres una asociación sube a tus peludos para darles más difusion';
        onboardingSmallText = null;
    }

    return (
        <div className="onboarding">
            
            <span className="onboarding__close">
                <Link to="/select-login">
                    <img className="onboarding__close__img" src={Close} alt="close"/>
                </Link>
            </span>
            <img className="onboarding__logo" alt="picture_onboarding" src={onboardingLogo}></img>
            <h3 className="onboarding__text">{onboardingText}</h3>

            {onboardingSmallText ? (
                <p className="onboarding__smallText">{onboardingSmallText}</p>
            ) : null}

            <div className="onboarding__dots">
                <img className="onboarding__dots__img" alt="icon_dot" onClick={() => setActive('one')} src={active==='one' ? RedDot : Dot}></img>
                <img className="onboarding__dots__img" alt="icon_dot" onClick={() => setActive('two')} src={active==='two' ? RedDot : Dot}></img>
                <img className="onboarding__dots__img" alt="icon_dot" onClick={() => setActive('three')} src={active==='three' ? RedDot : Dot}></img>
            </div>
        </div>
    )
}

export default Onboarding;