import { useSelector } from 'react-redux';

import ProfileButton from "../../components/ProfileButton";

import User from "../../icons_USED/green-user.png";
import Dog from "../../icons_USED/mascota.png";
import Heart from "../../icons_USED/favoritosCopy.png";
import Apadrina from "../../icons_USED/apadrina.png";
import Donate from "../../icons_USED/donar.png";
import Notification from "../../icons_USED/notificaciones.png";
import Localization from "../../icons_USED/localization.png";

import "./Profile.scss";

function Profile() {

  const user = useSelector(state => state.user.user);

  return (
    <div className="profile">
      <span className="profile__foto">
        <img
          className="profile__foto__img"
          src={user.picture}
          alt="profile user"
        ></img>
      </span>

      <div className="profile__box">
        <ProfileButton Icon={User} Name="Mi perfil" />
        <ProfileButton Icon={Localization} Name="Direcciones" />
        <ProfileButton Icon={Heart} Name="Favoritos" />
        <ProfileButton Icon={Notification} Name="Notificaciones" />

        <div className="profile__box__separation-button"></div>

        <ProfileButton Icon={Dog} Name="Estado de la adopción" />
        <ProfileButton Icon={Apadrina} Name="Apadrinar" />
        <ProfileButton Icon={Donate} Name="Donar" />

      </div>
    </div>
  );
}

export default Profile;
