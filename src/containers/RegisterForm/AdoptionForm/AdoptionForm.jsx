import React, { useState } from "react";
import { Redirect } from "react-router-dom";
import Data from "./components/Data";
import AboutPets from "./components/AboutPets";
import FamilyHome from "./components/FamilyHome";
import Submitted from "./components/Submitted";

import Arrow from "../../../icons_USED/arrow-back.png";

import "./AdoptionForm.scss";

function AdoptionForm() {
  const data = <Data />;
  const pets = <AboutPets />;
  const family = <FamilyHome />;
  const modal = <Submitted />;
  const components = [data, pets, family];

  const [counter, setCounter] = useState(0);
  const [component, setComponent] = useState(components[0]);
  const [advertissement, setAdvertissement] = useState(false);
  const [redirect, setRedirect] = useState(false);

  function nextComponent(counter) {
    const nextPage = counter + 1;
    setCounter(nextPage);
    if (counter <= 3) {
      setComponent(components[nextPage]);
    }
    if (counter === 2 && advertissement === false) {
      setAdvertissement(modal);
      setComponent(components[2]);
    } else {
      setAdvertissement(false);
    }
  }

  function previousComponent(counter) {
    const previousPage = counter - 1;
    setCounter(previousPage);
    if (counter > 0) {
      setComponent(components[previousPage]);
    } else {
      if (counter === 0 && redirect === false) {
        setRedirect(<Redirect to="/adoption" />);
      }
    }
  }

  return (
    <div className="content">
      <div className="title-container-AF">
        <button
          className="icon-atras"
          onClick={() => previousComponent(counter)}
        >
          <img src={Arrow} alt="goback" />
          {redirect}
        </button>
        <p>Formulario de adopción</p>
      </div>

      {component}
      {advertissement}

      <button
        type="submit"
        className="btn-continue-AF"
        onClick={() => nextComponent(counter)}
      >
        Continuar
      </button>

    </div>
  );
}

export default AdoptionForm;
