import React from "react";
import { useDispatch, useSelector } from 'react-redux';
import { setAdoptionSlice } from '../../../../app/slices/adoptionSlice';
import "../AdoptionForm.scss";

function AboutPets() {

  const dispatch = useDispatch();
  const form = useSelector(state => state.adoption.form);

  function handleChangeInput (ev)  {
    
    const name = ev.target.name;
    const value = ev.target.value;

    const [section, fieldname] = name.split('.')

    dispatch(setAdoptionSlice({
      form:{
        ...form,
        infoAnimal:{
          ...form.infoAnimal,
          [fieldname]: value,
        }
      }
    }))
  }

  return (
    <div className="component-form">
      <div className="progress">
        <span className="progress-bar-AP" />
        <span className="remaining-progress-AP" />
      </div>

      <div className="component-form">
        <div className="form-container-AF">
          <h3>Sobre las mascotas</h3>

          <div className="other-animals-AP">
            <h3>¿Tienes otros animales?</h3>
            <div className="radio-container-AP">
              <p onChange={handleChangeInput}>
                <input 
                type="radio" 
                id="otherAnimals_yes"
                name="infoAnimal.otherAnimals"
                value="Si"
                />
                <label htmlFor="otherAnimals_yes">
                  <span></span>Sí
                </label>
              </p>
            </div>
            <div className="radio-container-AP">
              <p onChange={handleChangeInput}>
                <input 
                type="radio" 
                id="otherAnimals_no" 
                name="infoAnimal.otherAnimals"
                value="No"
                />
                <label htmlFor="otherAnimals_no" >
                  <span></span>No
                </label>
              </p>
            </div>
          </div>

          <div className="input-container-AP">
            <input
              type="text"
              className="input-form-AP"
              placeholder="¿Cuáles?"
              onChange={handleChangeInput} 
              name="infoAnimal.animals"
              value={form.infoAnimal.animals}
            />
          </div>

          <div className="input-container-AP">
            <input
              type="text"
              className="input-form-AP"
              placeholder="¿Se llevan bien con otros animales?"
              onChange={handleChangeInput} 
              name="infoAnimal.petFriendly"
              value={form.infoAnimal.petFriendly}
            />
          </div>

          <div className="input-container-AP-2">
            <p className="title-input-AP">¿Por qué has elegido adoptar?</p>
            <input type="text" 
            className="input-form-AP" 
            onChange={handleChangeInput} 
            name="infoAnimal.whyAdopt"
            value={form.infoAnimal.whyAdopt}
            />
          </div>

          <div className="input-container-AP-2">
            <p className="title-input-AP">
              ¿Conoces las necesidades del animal?
            </p>
            <input 
            type="text" 
            className="input-form-AP" 
            onChange={handleChangeInput} 
            name="infoAnimal.animalNeeds"
            value={form.infoAnimal.animalNeeds}
            />
          </div>

          <div className="input-container-AP-2">
            <p className="title-input-AP">¿Conoces sus gastos?</p>
            <input 
            type="text" 
            className="input-form-AP" 
            onChange={handleChangeInput} 
            name="infoAnimal.expenses"
            value={form.infoAnimal.expenses}
            />
          </div>

          <div className="input-container-AP-2">
            <p className="title-input-AP">¿Conoces su alimentación?</p>
            <input 
            type="text" 
            className="input-form-AP" 
            onChange={handleChangeInput} 
            name="infoAnimal.food"
            value={form.infoAnimal.food}
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default AboutPets;
