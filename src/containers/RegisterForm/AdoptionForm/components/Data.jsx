import React from "react";
import { useDispatch, useSelector } from 'react-redux';
import { setAdoptionSlice } from '../../../../app/slices/adoptionSlice';

function Data() {

  const dispatch = useDispatch();
  const form = useSelector(state => state.adoption.form);

  function handleChangeInput (ev)  {
    
    const name = ev.target.name;
    const value = ev.target.value;

    const [section, fieldname] = name.split('.')

    dispatch(setAdoptionSlice({
      ...form,
      form:{
        ...form,
        personalData:{
          ...form.personalData,
          [fieldname]: value,
        }
      }
    }))
  }

  return (
    <div className="component-form">
      <div className="progress">
        <span className="progress-bar-AF" />
        <span className="remaining-progress-AF" />
      </div>
      <div className="form-container-AF">
        <h3>Tus datos</h3>

        <div className="input-container-AF">
          <input
            type="text"
            className="input-form-AF"
            placeholder="Nombre y apellidos"
            onChange={handleChangeInput} 
            name="personalData.name"
            value={form.personalData.name}
          />
        </div>

        <div className="input-container-AF">
          <input 
          type="text" 
          className="input-form-AF" 
          placeholder="Email" 
          onChange={handleChangeInput} 
          name="personalData.email"
          value={form.personalData.email}
          />
        </div>

        <div className="input-container-AF">
          <input 
          type="text" 
          className="input-form-AF" 
          placeholder="Teléfono"
          onChange={handleChangeInput} 
          name="personalData.phone"
          value={form.personalData.phone} />
        </div>

        <div className="input-container-AF">
          <input 
          type="text" 
          className="input-form-AF" 
          placeholder="DNI"
          onChange={handleChangeInput} 
          name="personalData.dni"
          value={form.personalData.dni} />
        </div>

        <h3>Dirección</h3>

        <div className="input-container-AF">
          <input
            type="text"
            className="input-form-AF"
            placeholder="Calle, número, piso"
            onChange={handleChangeInput} 
            name="personalData.address"
            value={form.personalData.address}
          />
        </div>

        <div className="input-container-AF">
          <input
            type="text"
            className="input-form-AF"
            placeholder="Código postal"
            onChange={handleChangeInput} 
            name="personalData.postalCode"
            value={form.personalData.postalCode}
          />
        </div>

        <div className="input-container-AF">
          <input 
          type="text" 
          className="input-form-AF" 
          placeholder="Ciudad"
          onChange={handleChangeInput} 
          name="personalData.city"
          value={form.personalData.city} 
          />
        </div>
      </div>

      <div className="terms-container-AF">
        <p>
          <input id="checkbox-AF" type="checkbox" />
          <label htmlFor="checkbox-AF">
            <span></span>Acepto los términos y condiciones de la adopción
          </label>
        </p>
      </div>
    </div>
  );
}

export default Data;
