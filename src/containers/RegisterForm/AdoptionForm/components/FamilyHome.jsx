import React from "react";
import { useDispatch, useSelector } from 'react-redux';
import { setAdoptionSlice } from '../../../../app/slices/adoptionSlice';
import "../AdoptionForm.scss";

function FamilyHome(props) {

  const dispatch = useDispatch();
  const form = useSelector(state => state.adoption.form);

  function handleChangeInput (ev)  {
    
    const name = ev.target.name;
    const value = ev.target.value;

    const [section, fieldname] = name.split('.')

    dispatch(setAdoptionSlice({
      form:{
        ...form,
        infoFamily:{
          ...form.infoFamily,
          [fieldname]: value,
        }
      }
    }))
  }


  return (
    <div className="component-form">
      <div className="progress">
        <span className="progress-bar-FH" />
        <span className="remaining-progress-FH" />
      </div>
      <div className="component-form">
        <div className="form-container-AF">
          <h3>Familia y hogar</h3>

          <div className="input-container-AP-2">
            <p className="title-input-AP">¿Donde vives?</p>
            <input
              type="text"
              className="input-form-AP"
              placeholder="Piso, casa, chalet..."
              onChange={handleChangeInput}
              name="infoFamily.typeHome"
              value={form.infoFamily.typeHome}
            />
          </div>

          <div className="other-animals-AP" >
            <h3>¿Vives de alquiler?</h3>
            <div className="radio-container-AP">
              <p onChange={handleChangeInput}>
                <input 
                type="radio" 
                id="rent_yes"
                name="infoFamily.rent"
                value="Si"
                />
                <label htmlFor="rent_yes">
                  <span></span>Sí
                </label>
              </p>
            </div>
            <div className="radio-container-AP">
              <p onChange={handleChangeInput}>
                <input 
                type="radio" 
                id="rent_no" 
                name="infoFamily.rent"
                value="No"
                />
                <label htmlFor="rent_no">
                  <span></span>No
                </label>
              </p>
            </div>
          </div>

          <div className="other-animals-AP">
            <h3>¿Tu casero permite animales?</h3>
            <div className="radio-container-AP">
              <p onChange={handleChangeInput}>
                <input 
                type="radio" 
                id="allowPets_yes"
                name="infoFamily.allowPets"
                value="Si"
                />
                <label htmlFor="allowPets_yes">
                  <span></span>Sí
                </label>
              </p>
            </div>
            <div className="radio-container-AP">
              <p onChange={handleChangeInput}>
                <input 
                type="radio" 
                id="allowPets_no"
                name="infoFamily.allowPets"
                value="No"
                />
                <label htmlFor="allowPets_no">
                  <span></span>No
                </label>
              </p>
            </div>
          </div>

          <div className="other-animals-AP">
            <h3>¿Crees que podrías mudarte pronto?</h3>
            <div className="radio-container-AP">
              <p onChange={handleChangeInput}>
                <input 
                type="radio" 
                id="changeHome_yes"
                name="infoFamily.changeHome"
                value="Si"
                />
                <label htmlFor="changeHome_yes">
                  <span></span>Sí
                </label>
              </p>
            </div>
            <div className="radio-container-AP">
              <p onChange={handleChangeInput}>
                <input 
                type="radio" 
                id="changeHome_no"
                name="infoFamily.changeHome"
                value="No"
                />
                <label htmlFor="changeHome_no">
                  <span></span>No
                </label>
              </p>
            </div>
          </div>

          <div className="other-animals-AP">
            <h3>¿Tiene Jardín?</h3>
            <div className="radio-container-AP">
              <p onChange={handleChangeInput}>
                <input 
                type="radio" 
                id="rental_yes"
                name="infoFamily.garden"
                value="Si"
                />
                <label htmlFor="rental_yes">
                  <span></span>Sí
                </label>
              </p>
            </div>
            <div className="radio-container-AP">
              <p onChange={handleChangeInput}>
                <input 
                type="radio" 
                id="rental_no" 
                name="infoFamily.garden"
                value="No"
                />
                <label htmlFor="rental_no">
                  <span></span>No
                </label>
              </p>
            </div>
          </div>

          <div className="other-animals-AP">
            <h3>¿Vives con otras personas?</h3>
            <div className="radio-container-AP">
              <p onChange={handleChangeInput}>
                <input 
                type="radio" 
                id="other_people_yes"
                name="infoFamily.otherPeople"
                value="Si"
                />
                <label htmlFor="other_people_yes">
                  <span></span>Sí
                </label>
              </p>
            </div>
            <div className="radio-container-AP">
              <p onChange={handleChangeInput}>
                <input 
                type="radio" 
                id="other_people_no"
                name="infoFamily.otherPeople"
                value="No"
                />
                <label htmlFor="other_people_no">
                  <span></span>No
                </label>
              </p>
            </div>
          </div>

          <div className="other-animals-AP">
            <h3>¿Están todos de acuerdo con la adopción?</h3>
            <div className="radio-container-AP">
              <p onChange={handleChangeInput}>
                <input 
                type="radio" 
                id="all_yes"
                name="infoFamily.wantAdoption"
                value="Si"
                />
                <label htmlFor="all_yes">
                  <span></span>Sí
                </label>
              </p>
            </div>
            <div className="radio-container-AP">
              <p onChange={handleChangeInput}>
                <input 
                type="radio" 
                id="all_no"
                name="infoFamily.wantAdoption"
                value="No"
                />
                <label htmlFor="all_no">
                  <span></span>No
                </label>
              </p>
            </div>
          </div>

          <div className="other-animals-AP">
            <h3>¿Estás de acuerdo con que visitemos tu casa?</h3>
            <div className="radio-container-AP">
              <p onChange={handleChangeInput}>
                <input 
                type="radio" 
                id="visit_yes"
                name="infoFamily.visitHome" 
                value="Si"
                />
                <label htmlFor="visit_yes">
                  <span></span>Sí
                </label>
              </p>
            </div>
            <div className="radio-container-AP">
              <p onChange={handleChangeInput}>
                <input 
                type="radio" 
                id="visit_no"
                name="infoFamily.visitHome" 
                value="No"
                />
                <label htmlFor="visit_no">
                  <span></span>No
                </label>
              </p>
            </div>
          </div>
        </div>
      </div>
      
        
    </div>
  );
}

export default FamilyHome;
