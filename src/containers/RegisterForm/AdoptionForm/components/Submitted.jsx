import React from "react";
import { Link } from "react-router-dom";
import Ilustration from "../../../../images/adoptionform/cat.png";
import "../AdoptionForm.scss";

function Submitted() {
  return (
    <div className="floating-box-SF">
      <div className="pop-up-container">
        <div className="close-pop-up">
          <Link className="link" to="/adoption">
            <h3>X</h3>
          </Link>
        </div>

        <h2>¡Enviado!</h2>
        <p>
          Hemos enviado tu formulario a la protectora. Si quieres ponerte en
          contacto con ellos puedes hacerlo vía email o whatsapp.
        </p>
        <p>
          Recuerda que la protectora se pondrá en contacto contigo para poder
          hacer la entrevista personal.
        </p>

        <img src={Ilustration} alt="cat-ilustration" />
      </div>
    </div>
  );
}

export default Submitted;
