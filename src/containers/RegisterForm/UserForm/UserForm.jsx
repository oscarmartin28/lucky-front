import { Link } from 'react-router-dom';
import { useState } from 'react'
import { useDispatch } from 'react-redux';

import { register } from '../../../api/auth';
import { setUserSlice } from '../../../app/slices/userSlice';

import Camera from '../../../icons_USED/green-boy.png';

import './UserForm.scss';

const INITIAL_STATE = {

    email: '',
    username: '',
    picture: '',
    password: '',
    error: '',
}
 
function UserForm(props) {

    const dispatch = useDispatch();

    const [state, setState] = useState(INITIAL_STATE);

    const [ preview, setPreview ] = useState(null);

    async function handleSubmitForm (ev) {
        ev.preventDefault();
           const { history } = props;
        try {
            const formData = new FormData();

            formData.append('picture', state.picture);
            formData.append('username', state.username);
            formData.append('email', state.email);
            formData.append('password', state.password);

            const data = await register(formData);
            dispatch(setUserSlice({
                user: data,
                hasUser: true,
            }));
            setState(INITIAL_STATE);
            history.push('/login');

        }catch(error) {
            setState({error: error.message});
        };
    };

    function handleChangeInput (ev)  {
        const name = ev.target.name;
        const value = ev.target.value;

        if (name === 'picture') {
            let reader = new FileReader();
            let file = ev.target.files[0];

            reader.onloadend = () => {
                setState({
                    ...state,
                    picture: file,
                });
                setPreview(reader.result);
            }
            reader.readAsDataURL(file);
        }else {
            setState({
                ...state,
                [name]: value,
            });
        }

    };

    return (
        <div className="userForm">
            <div className="userForm__box">
                <div className="userForm__box__img" >
                    <img className="userForm__box__img__image" src="/img/logo/logo-M.png" alt="logo lucky"/>
                </div>
                <h4 className="title__principal">¡Hola! para continuar, crea una cuenta</h4>
                <form className="userForm__form" onSubmit={handleSubmitForm} encType='multipart/form-data'>

                    {preview &&
                        <div className="userForm__form__label__boximg">
                            <img className="userForm__form__label__boximg__img" src={preview} alt="user"/>
                        </div>}

                    <label className="userForm__form__label--picture" htmlFor="picture">
                        <input 
                        className="userForm__form__label__input--picture"
                        type="file" 
                        name="picture"
                        id="picture"
                        onChange={handleChangeInput}
                        />
                        <img className="userForm__form__label--picture__icon"  src={Camera} alt="camera"/>
                        <p className="title__principal--medium" >Subir Foto</p>
                    </label>


                    <label className="userForm__form__label" htmlFor="username">
                        <input 
                        className="userForm__form__label__input" 
                        type="text" 
                        name="username" 
                        value={state.username} 
                        onChange={handleChangeInput} 
                        placeholder="Claudia"
                        />
                    </label>

                    <label className="userForm__form__label" htmlFor="email">
                        <input 
                        className="userForm__form__label__input"  
                        type="text" 
                        name="email" 
                        value={state.email} 
                        onChange={handleChangeInput} 
                        placeholder="claudia@email.com"
                        />
                    </label>
                    
                    <label className="userForm__form__label" htmlFor="password">
                        <input 
                        className="userForm__form__label__input" 
                        type="password" name="password" 
                        value={state.password} 
                        onChange={handleChangeInput} 
                        placeholder="*********"
                        />
                    </label>

                    {state.error && <p className="error">{state.error}</p>}

                    <div className="userForm__form__button">
                        <button className="button-principal" type="submit">Crear Cuenta</button>
                    </div>
                </form>
                <Link className="link" to="/select-login">Volver</Link>
            </div>
        </div>
    )
}

export default UserForm;