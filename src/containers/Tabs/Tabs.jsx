import { useState } from "react";

import Paw from '../../icons_USED/green-paw.png';
import Info from '../../icons_USED/red-info.png';
import Close from '../../icons_USED/green-close.png';

import "./Tabs.scss";

function Tabs(props) {

  const { species, birthday, gender, size, weight, personality, story } = props.pets.location.state.info;
  let { vaccinated, dewormed, healthy, identified, microchip, toKnow } = props.pets.location.state.health;
  const { requirements, rate, shippingCity } = props.pets.location.state.adoption;

  const [isAddInfo, setIsAddInfo] = useState(false);

  const closeInfo = () => {
    setIsAddInfo(false);
    }

  const [toggleState, setToggleState] = useState(1);

  const toggleTab = (index) => {
    setToggleState(index);
  };

  return (
    <div className="tabs__container">
      <div className="tabs__container__tabs">
        <button
          className={toggleState === 1 ? "tabs active-tabs" : "tabs"}
          onClick={() => toggleTab(1)}
        >
          Datos
        </button>
        <button
          className={toggleState === 2 ? "tabs active-tabs" : "tabs"}
          onClick={() => toggleTab(2)}
        >
          Salud
        </button>
        <button
          className={toggleState === 3 ? "tabs active-tabs" : "tabs"}
          onClick={() => toggleTab(3)}
        >
          Adopción
        </button>
      </div>

      <div className="tabs__container__content-tabs">
        <div
          className={toggleState === 1 ? "content__tab  active-content" : "content__tab"}
        >
            <div className="tabs__container__tab">
                <div className="tabs__container__tab__item">
                    <span className="tabs__container__tab__item__span">
                        <img className="tabs__container__tab__item__span__img" src={Paw} alt="paw"/>
                    </span>
                    <div className="tabs__container__tab__item__data">
                        <div className="tabs__container__tab__item__data__title">
                            <p>Especie:</p>
                        </div>
                        <div className="tabs__container__tab__item__data__subtitle--specie">
                            <p>{species}</p>
                        </div>
                    </div>                 
                </div>
                <div className="tabs__container__tab__item">
                    <span className="tabs__container__tab__item__span">
                        <img className="tabs__container__tab__item__span__img" src={Paw} alt="paw"/>
                    </span>
                    <div className="tabs__container__tab__item__data">
                        <div className="tabs__container__tab__item__data__title">
                            <p>Nacimiento:</p>
                        </div>
                        <div className="tabs__container__tab__item__data__subtitle">
                            <p>{birthday}</p>
                        </div>
                    </div>                 
                </div>
                <div className="tabs__container__tab__item">
                    <span className="tabs__container__tab__item__span">
                        <img className="tabs__container__tab__item__span__img" src={Paw} alt="paw"/>
                    </span>
                    <div className="tabs__container__tab__item__data">
                        <div className="tabs__container__tab__item__data__title">
                            <p>Sexo:</p>
                        </div>
                        <div className="tabs__container__tab__item__data__subtitle">
                            <p>{gender}</p>
                        </div>
                    </div>                 
                </div>
                <div className="tabs__container__tab__item">
                    <span className="tabs__container__tab__item__span">
                        <img className="tabs__container__tab__item__span__img" src={Paw} alt="paw"/>
                    </span>
                    <div className="tabs__container__tab__item__data">
                        <div className="tabs__container__tab__item__data__title">
                            <p>Tamaño:</p>
                        </div>
                        <div className="tabs__container__tab__item__data__subtitle">
                            <p>{size}</p>
                        </div>
                    </div>                 
                </div>
                <div className="tabs__container__tab__item">
                    <span className="tabs__container__tab__item__span">
                        <img className="tabs__container__tab__item__span__img" src={Paw} alt="paw"/>
                    </span>
                    <div className="tabs__container__tab__item__data">
                        <div className="tabs__container__tab__item__data__title">
                            <p>Peso:</p>
                        </div>
                        <div className="tabs__container__tab__item__data__subtitle">
                            <p>{weight} kg</p>
                        </div>
                    </div>                 
                </div>
            </div>

            <div className="tabs__container__section">
                <h4 className="title__principal--medium__left">Personalidad</h4>
                <p className="tabs__container__section__text">{personality}</p>
            </div>

            <div className="tabs__container__section2">
                <h4 className="title__principal--medium__left">Historia</h4>
                <p className="tabs__container__section__text2">{story}</p>
            </div>
        </div>

        <div
          className={toggleState === 2 ? "content__tab  active-content" : "content__tab"}
        >
                      <div className="tabs__container__tab">
                <div className="tabs__container__tab__item">
                    <span className="tabs__container__tab__item__span">
                        <img className="tabs__container__tab__item__span__img" src={Paw} alt="paw"/>
                    </span>
                    <div className="tabs__container__tab__item__data">
                        <div className="tabs__container__tab__item__data__title">
                            <p>Vacunado:</p>
                        </div>
                        <div className="tabs__container__tab__item__data__subtitle">
                            <p>{vaccinated.toString() === 'true' ? vaccinated = 'Si' : vaccinated = 'No'}</p>
                        </div>
                    </div>                 
                </div>
                <div className="tabs__container__tab__item">
                    <span className="tabs__container__tab__item__span">
                        <img className="tabs__container__tab__item__span__img" src={Paw} alt="paw"/>
                    </span>
                    <div className="tabs__container__tab__item__data">
                        <div className="tabs__container__tab__item__data__title">
                            <p>Desparasitado:</p>
                        </div>
                        <div className="tabs__container__tab__item__data__subtitle">
                            <p>{dewormed.toString() === 'true' ? dewormed = 'Si' : dewormed = 'No'}</p>
                        </div>
                    </div>                 
                </div>
                <div className="tabs__container__tab__item">
                    <span className="tabs__container__tab__item__span">
                        <img className="tabs__container__tab__item__span__img" src={Paw} alt="paw"/>
                    </span>
                    <div className="tabs__container__tab__item__data">
                        <div className="tabs__container__tab__item__data__title">
                            <p>Sano:</p>
                        </div>
                        <div className="tabs__container__tab__item__data__subtitle">
                            <p>{healthy.toString() === 'true' ? healthy = 'Si' : healthy = 'No'}</p>
                        </div>
                    </div>                 
                </div>
                <div className="tabs__container__tab__item">
                    <span className="tabs__container__tab__item__span">
                        <img className="tabs__container__tab__item__span__img" src={Paw} alt="paw"/>
                    </span>
                    <div className="tabs__container__tab__item__data">
                        <div className="tabs__container__tab__item__data__title">
                            <p>Identificado:</p>
                        </div>
                        <div className="tabs__container__tab__item__data__subtitle">
                            <p>{identified.toString() === 'true' ? identified = 'Si' : identified = 'No'}</p>
                        </div>
                    </div>                 
                </div>
                <div className="tabs__container__tab__item">
                    <span className="tabs__container__tab__item__span">
                        <img className="tabs__container__tab__item__span__img" src={Paw} alt="paw"/>
                    </span>
                    <div className="tabs__container__tab__item__data">
                        <div className="tabs__container__tab__item__data__title">
                            <p>Microchip:</p>
                        </div>
                        <div className="tabs__container__tab__item__data__subtitle">
                            <p>{microchip.toString() === 'true' ? microchip = 'Si' : microchip = 'No'}</p>
                        </div>
                    </div>                 
                </div>
            </div>
            <div className="tabs__container__section3">
                <h4 className="title__principal--small">Tienes que saber que...</h4>
                <p className="tabs__container__section3__text2">{toKnow}</p>
            </div>
        </div>

        <div
          className={toggleState === 3 ? "content__tab  active-content" : "content__tab"}
        >
            <div className="tabs__container__section2">
                <h4 className="title__principal--medium__left">Requisitos adopción</h4>
                <p className="tabs__container__section2__text2">{requirements}</p>
            </div>

            <div className="tabs__container__section2">
                <div className="tabs__container__section2__title">
                    <h4 className="title__principal--medium__left">Tasa adopción</h4>
                    <span className="tabs__container__section2__span">
                        <img  onClick={() => setIsAddInfo({ isAddInfo: true })} 
                        className="tabs__container__section2__span__img" 
                        src={Info} alt="info"/>
                    </span>
                    {isAddInfo && 
                    (<div className="modal">
                        <p className="modal__text">No están incluidos los gastos de envío.</p>
                        <span className="modal__span" onClick={closeInfo}>
                            <img className="modal__span__img" src={Close} alt="close"/>
                        </span>
                    </div>)}
                    
                </div>      
                <p className="tabs__container__section2__text2">{rate} €</p>
            </div>

            <div className="tabs__container__section2">
                <h4 className="title__principal--medium__left">Ciudad de Origen</h4>
                <p className="tabs__container__section2__text2">{shippingCity}</p>
            </div>
        </div>
      </div>
    </div>
  );
}

export default Tabs;
